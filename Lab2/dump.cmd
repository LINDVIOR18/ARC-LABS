@REM ----------------------------------------------------
@REM remotecap.cmd
@REM   Conectare server development si tunelare tcpdump in wireshark windows
@REM ----------------------------------------------------
@SET PLINK_PATH="C:\Program Files\PuTTY\plink.exe"
@SET WIRESHARK_PATH="C:\Program Files\Wireshark\Wireshark.exe"
@SET REMOTE_SERVER=212.47.235.142
@SET REMOTE_ACCOUNT=ubuntu
@SET REMOTE_cert="D:\Guniver\ARC\Lab2\cert4win.ppk"
@SET REMOTE_INTERFACE=ens2
@REM execute command
%PLINK_PATH% -ssh -i %REMOTE_cert% %REMOTE_ACCOUNT%@%REMOTE_SERVER% -no-antispoof "sudo tcpdump -s 0 -U -n -w - -i ens2 port 8002" | %WIRESHARK_PATH% -i - -k
