package com.lab.ark.repository.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "packet")
@Getter
@Setter
public class PacketEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "type")
    private String type;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "raw_data")
    private String rawData;

    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "packetEntity")
    private LocationEntity locationEntity;
}
