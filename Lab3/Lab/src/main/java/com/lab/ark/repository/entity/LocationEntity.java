package com.lab.ark.repository.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "location")
@Getter
@Setter
public class LocationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "date")
    private String date;

    @Column(name = "time")
    private String time;

    @Column(name = "position_function")
    private String positionFunction;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "speed")
    private String speed;

    @Column(name = "direction")
    private String direction;

    @Column(name = "altitude")
    private String altitude;

    @Column(name = "satellite")
    private String satellite;

    @Column(name = "gsm_signal_strength")
    private String gsmSignalStrength;

    @Column(name = "battery")
    private String battery;

    @Column(name = "pedometer")
    private String pedometer;

    @Column(name = "rolling_times")
    private String rollingTimes;

    @Column(name = "terminal_statement")
    private String terminalStatement;

    @Column(name = "base_station_number")
    private String baseStationNumber;

    @Column(name = "connect_delay")
    private String connectDelay;

    @Column(name = "m_c_c_country_code")
    private String mCCCountryCode;

    @Column(name = "m_n_c_network")
    private String mNCNetwork;

    @Column(name = "area_code")
    private String areaCode;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "signal_strength")
    private String signalStrength;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "packet_id", nullable = false)
    private PacketEntity packetEntity;
}

