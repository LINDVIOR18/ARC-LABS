package com.lab.ark.service;

import com.lab.ark.service.dto.PacketDto;

public interface PacketService {

    void create(PacketDto packetDto);

}
