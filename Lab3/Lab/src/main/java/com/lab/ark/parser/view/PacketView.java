package com.lab.ark.parser.view;

import com.lab.ark.repository.entity.LocationEntity;
import lombok.Data;

import java.io.Serializable;

@Data
public class PacketView implements Serializable {

    private static final long serialVersionUID = 1869169124669723044L;

    private Long id;
    private String type;
    private String companyName;
    private String deviceId;
    private String rawData;
    private LocationEntity locationEntity;
}
