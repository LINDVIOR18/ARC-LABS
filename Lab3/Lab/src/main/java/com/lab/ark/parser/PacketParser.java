package com.lab.ark.parser;

import com.lab.ark.parser.view.LocationView;
import com.lab.ark.parser.view.PacketView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PacketParser {

    private final PacketView packetView;
    private final LocationView locationView;

    //    public void parsePacket(String packet) {
    public static void main(String[] args) {
        String type = "UD2";

        String packet = "[3G*1204743213*00AD*UD2,300920,150258,V,47.015870,N,28.8255417,E,0.00,0.0,0.0,0,100,53,0,0,00000008,6,0,259,1,215,8522,156,215,1363,143,215,1361,135,215,24102,133,218,9971,127,218,23,126,0,61.3]";
        String[] packetSplit = packet.replaceAll("[\\[\\]]", "")
                .replaceAll(",", "*")
                .split("\\*");
        System.out.println("Company Name: " + packetSplit[0]);
        System.out.println("Device Id: " + packetSplit[1]);
        System.out.println("Raw Data: " + packetSplit[2]);
        System.out.println("Type: " + packetSplit[3]);
    }
//    String type = "UD2";
//
//        String[] packetSplit = packet.replaceAll("[\\[\\]]", "")
//                .replaceAll(",", "*")
//                .split("\\*");


//        packetView.setCompanyName(packetSplit[0]);
//        packetView.setDeviceId(packetSplit[1]);
//        packetView.setRawData(packetSplit[2]);
//        packetView.setType(packetSplit[3]);

        // Not Finished...
//
//        if (packetSplit[3].equals(type)) {
//            locationView.s

//        }
//    }
}
