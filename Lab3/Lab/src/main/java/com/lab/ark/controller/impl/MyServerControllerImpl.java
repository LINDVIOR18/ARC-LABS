package com.lab.ark.controller.impl;

import com.lab.ark.controller.MyServerController;
import com.lab.ark.service.MyServerSocket;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class MyServerControllerImpl implements MyServerController {

    private final MyServerSocket myServerSocket;
    @Override
    public void setPortToListening(int port) {
        myServerSocket.connectionTCP(port);
    }
}
