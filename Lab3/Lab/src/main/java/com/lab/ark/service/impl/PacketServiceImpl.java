package com.lab.ark.service.impl;

import com.lab.ark.repository.PacketRepository;
import com.lab.ark.repository.entity.PacketEntity;
import com.lab.ark.service.PacketService;
import com.lab.ark.service.convertor.PacketConverter;
import com.lab.ark.service.dto.PacketDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PacketServiceImpl implements PacketService {

    private final PacketRepository packetRepository;
    private final PacketConverter packetConverter;

    @Override
    public void create(PacketDto packetDto) {

        PacketEntity entity = packetConverter.deconvert(packetDto);
        packetRepository.save(entity);
    }
}
