package com.lab.ark.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/server")
@Api(value = "Server Controller REST Endpoint")
public interface MyServerController {

    @ApiOperation(value = "Get port address")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Port is accepted"),
            @ApiResponse(code = 406, message = "Port is not accepted ...")
    })
    @GetMapping
    void setPortToListening(int port);
}
