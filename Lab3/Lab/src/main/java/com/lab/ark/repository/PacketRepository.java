package com.lab.ark.repository;

import com.lab.ark.repository.entity.PacketEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacketRepository extends JpaRepository<PacketEntity, Long> {
}
