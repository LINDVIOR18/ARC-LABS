package com.lab.ark.service.dto;

import com.lab.ark.repository.entity.PacketEntity;
import lombok.Data;

@Data
public class LocationDto {
    private Long id;
    private String date;
    private String time;
    private String positionFunction;
    private String latitude;
    private String longitude;
    private String speed;
    private String direction;
    private String altitude;
    private String satellite;
    private String gsmSignalStrength;
    private String battery;
    private String pedometer;
    private String rollingTimes;
    private String terminalStatement;
    private String baseStationNumber;
    private String connectDelay;
    private String mCCCountryCode;
    private String mNCNetwork;
    private String areaCode;
    private String serialNumber;
    private String signalStrength;
    private PacketEntity packetEntity;
}

