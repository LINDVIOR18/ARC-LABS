package com.lab.ark.service.convertor;

import com.lab.ark.repository.entity.PacketEntity;
import com.lab.ark.service.dto.PacketDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PacketConverter implements Converter<PacketEntity, PacketDto> {
    @Override
    public PacketDto convert(PacketEntity packetEntity) {
        if (packetEntity == null) return null;
        PacketDto dto = new PacketDto();
        dto.setId(packetEntity.getId());
        dto.setDeviceId(packetEntity.getDeviceId());
        dto.setCompanyName(packetEntity.getCompanyName());
        dto.setRawData(packetEntity.getRawData());
        dto.setType(packetEntity.getType());

        return dto;
    }

    public PacketEntity deconvert(PacketDto dto) {
        if (dto == null) return null;
        PacketEntity entity = new PacketEntity();
        entity.setId(dto.getId());
        entity.setDeviceId(dto.getDeviceId());
        entity.setCompanyName(dto.getCompanyName());
        entity.setRawData(dto.getRawData());
        entity.setType(dto.getType());

        return entity;
    }
}
