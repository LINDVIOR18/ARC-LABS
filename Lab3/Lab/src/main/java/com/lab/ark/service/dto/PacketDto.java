package com.lab.ark.service.dto;

import com.lab.ark.repository.entity.LocationEntity;
import lombok.Data;

@Data
public class PacketDto {

    private Long id;
    private String type;
    private String companyName;
    private String deviceId;
    private String rawData;
    private LocationEntity locationEntity;

}
