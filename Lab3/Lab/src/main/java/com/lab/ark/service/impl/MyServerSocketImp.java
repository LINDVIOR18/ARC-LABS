package com.lab.ark.service.impl;

import com.lab.ark.parser.PacketParser;
import com.lab.ark.service.MyServerSocket;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

@Service
public class MyServerSocketImp implements MyServerSocket {

    public PacketParser packetParser;

    private static String getStringFromStream(InputStream crunchifyStream) throws IOException {
        System.out.println("inside getStringFromStream()");
        if (crunchifyStream != null) {
            Writer crunchifyWriter = new StringWriter();

            char[] crunchifyBuffer = new char[2048];
            try (crunchifyStream) {
                Reader crunchifyReader = new BufferedReader(new InputStreamReader(crunchifyStream, StandardCharsets.UTF_8));
                int count;
                while ((count = crunchifyReader.read(crunchifyBuffer)) != -1) {
                    crunchifyWriter.write(crunchifyBuffer, 0, count);
                }
            }
            return crunchifyWriter.toString();
        } else {
            return "";
        }
    }

    @Override
    public void connectionTCP(int port) {

        String watchResponse = "";
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            System.out.println("Server is listening on port " + port);

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("New client connected");

                InputStream input = socket.getInputStream();
                watchResponse = getStringFromStream(input);
                input.close();

//                packetParser.parsePacket(watchResponse);
            }
        } catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}


