CREATE TABLE location
(
    id                  SERIAL PRIMARY KEY,
    packet_id           int,
    date                varchar(50),
    time                varchar(50),
    position_function   varchar(50),
    latitude            varchar(50),
    longitude           varchar(50),
    speed               varchar(50),
    direction           varchar(50),
    altitude            varchar(50),
    satellite           varchar(50),
    gsm_signal_strength varchar(50),
    battery             varchar(50),
    pedometer           varchar(50),
    rolling_times       varchar(50),
    terminal_statement  varchar(50),
    base_station_number varchar(50),
    connect_delay       varchar(50),
    m_c_c_country_code  varchar(50),
    m_n_c_network       varchar(50),
    area_code           varchar(50),
    serial_number       varchar(50),
    signal_strength     varchar(50),
    CONSTRAINT fk_packet
    FOREIGN KEY (packet_id)
        REFERENCES packet (id)
);