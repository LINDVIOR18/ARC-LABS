CREATE TABLE packet
(
    id           SERIAL PRIMARY KEY,
    type         varchar(50),
    company_name varchar(50),
    device_id    varchar(50),
    raw_data     varchar(50)
);